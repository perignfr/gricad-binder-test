# Démo/tuto d'utilisation de binderhub Gricad

Sources :

- ce dépôt gitlab
- le binderhub : https://binderhub.univ-grenoble-alpes.fr/
  
  Connexion possible uniquement en vpn ou réseau interne (filaire UGA) pour l'instant.


## Etape 1 : préparer le dépôt

- un fichier requirements.txt avec les dépendances python nécessaires

- un répertoire notebooks qui contient les feuilles de calcul à afficher sur binder.


